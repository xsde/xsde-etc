#! /usr/bin/env bash

# Create XSD/e distribution.
#
# -test
# -j <jobs>
#

trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

test=n
jobs=22

while [ $# -gt 0 ]; do
  case $1 in
    -test)
      test=y
      shift
      ;;
    -j)
      shift
      jobs=$1
      shift
      ;;
    *)
      error "unknown option: $1"
      exit 1
      ;;
  esac
done

v=`cat xsde/version`
bv=`cat ../build/build/version`
cv=`cat ../cutl/libcutl/version.txt`
xfv=`cat libxsd-frontend/version.txt`

echo "packaging xsde-$v"
echo "EVERYTHING MUST BE COMMITTED!"

# prepare xsde-x.y.z
#
rm -rf xsde-$v
mkdir xsde-$v
cd xsde
git archive master | tar -x -C ../xsde-$v
cd ..
rm -f xsde-$v/.gitignore

# Copy generated documentation.
#
cd xsde/doc
make -j $jobs

files="xsde.1 xsde.xhtml cxx/parser/guide/cxx-parser-e-guide.* cxx/serializer/guide/cxx-serializer-e-guide.* cxx/hybrid/guide/cxx-hybrid-e-guide.*"

for f in $files; do
  rsync -aq $f ../../xsde-$v/doc/$f
  touch ../../xsde-$v/doc/$f
done

cd ../..

# Copy generated source files.
#
cd xsde/xsde
make -j $jobs

files="options.?xx cxx/options.?xx cxx/parser/options.?xx cxx/serializer/options.?xx cxx/hybrid/options.?xx"

for f in $files; do
  rsync -aq $f ../../xsde-$v/xsde/$f
  touch ../../xsde-$v/xsde/$f
done

cd ../..

# prepare xsde-x.y.z+dep
#
rm -rf xsde-$v+dep
mkdir xsde-$v+dep
cd xsde+dep
git archive master | tar -x -C ../xsde-$v+dep
cd ../xsde-$v+dep
rm -f .gitignore
rsync -aq --copy-unsafe-links ../xsde-$v/ xsde/
cd ..

# Copy libcult
#
rsync -aq --copy-unsafe-links ../cutl/libcutl-$cv/ xsde-$v+dep/libcutl/

# Copy libxsd-fe
#
rsync -aq --copy-unsafe-links libxsd-frontend-$xfv/ xsde-$v+dep/libxsd-frontend/

# Install build
#
make -C ../build/build-$bv install_inc_dir=`pwd`/xsde-$v+dep install

# Test the dep pack.
#
if [ $test = y ]; then
  rm -rf xsde-$v+dep.test
  rsync -aq --copy-unsafe-links xsde-$v+dep/ xsde-$v+dep.test/
  cd xsde-$v+dep.test
  make -j $jobs test
  cd ..
  rm -r xsde-$v+dep.test
fi

# Package
#
tar cfj xsde-$v.tar.bz2 xsde-$v
tar cfj xsde-$v+dep.tar.bz2 xsde-$v+dep

sha1sum xsde-$v.tar.bz2 >xsde-$v.tar.bz2.sha1
sha1sum xsde-$v+dep.tar.bz2 >xsde-$v+dep.tar.bz2.sha1
