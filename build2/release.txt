1. Finalize xsd and all dependencies.

   * Release libcutl, libxsd-frontend (note: cli is develop-only).

   * Review @@.
   * Update NEWS files.
   * Update bindist configs if necessary (e.g., generic linux config).
   * Update build2 toolchain version constraints.
   * Update copyright.
   * CI and make sure all good.
   * Publish to queue.stage.build2.org and make sure all good.
   * Push everything.

2. Release and publish to queue.stage.build2.org to build binary packages.

   * Release BUT DON'T PUSH in the dependency order:

     - Update version constraints to previouly released final version.
     - bdep relase --no-open
     - Make sure pre-generated documentation is up to date.
     - Build and test locally (bdep test -a).

   * Publish to queue.stage.build2.org.

   * Confirm all good and binaries are built.

   * Update and run `download` script to download and arrange source and
     binary packages.

     - Smoke-test Windows and Linux archives.

   * Copy packages over to download directory, make sure there is enough
     disk space on host to publish.

   * Copy over (or add new) README.cli and update.

3. Update web pages

   * Update "Compilers & Platforms" page.

   * Update "Download" page.

   * Update install-build2 page.

   * Look over other pages for any changes (new C++ contructs, new feature,
     etc).

   * Copy over updated documentation to projects/xsde/documentation/. Review
     with gitk for any unexpected differences.

     - strip <?xml ...> in .xhtml files

3. Publish and announce

   * Push git repository (remembering to push the tag).

   * bdep-publish all the packages and make sure queued builds are good.

     NO: copy from queue.stage.build2.org for archive stability (we've
     already copied them to download/).

   * Migrate packages from queue to public. Cleanup any old betas.

   * Publish binaries and updated web pages (regenerate .cli files).

     - Add release on GitHub (add links to packages, NEWS entries; use
       previous release as template).

   * Write release announcements and send to xsde-{users, announcements}.

   * Announce on #build2.

   * Add news entries to web pages/RSS (landing page and product).

   [*] Announce on r/cpp/, lobste.rs

4. Finish

   * Commit web pages.

   * Commit xsde-etc.

   [*] Update Homebrew formula.
